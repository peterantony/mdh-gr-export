## Boomi MDH Golden Record (GR) Export

## Overview
This utility extracts GRs from a Repo.

## Description
Utilises MDH  Query GR API https://help.boomi.com/bundle/hub/page/r-mdm-Query_Golden_Records.html

## HowTO
Run using `getGRs.sh -u MDHRepoUserName -p MDHRepoPassword -c MDHCloudURL -n UniverseID`

Example: `getGRs.sh -u mdh -p demo -c  https://c01-aus-local.hub.boomi.com -n 12345678-abcd-efgh-0123456789ab`

#### Make sure the file `req.xml.init` has the correct API request payload as by default it will export ALL GRs

## Prerequisites
- curl
- xmllint
- Python Library [xmlutils](https://pypi.org/project/xmlutils/1.1/)

## Output
This will create two files in `tmp` subdirectory:
1. GR.xml
2. GR.csv
