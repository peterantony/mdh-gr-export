#!/bin/bash

while getopts ":u:p:c:n:" options; do
	case "${options}" in
		u) _u=${OPTARG} ;;
		p) _p=${OPTARG} ;;
		c) _c=${OPTARG} ;;
		n) _uni=${OPTARG} ;;
		*) ;;
	esac
done

_WD=${PWD}
mkdir $_WD/tmp 2> /dev/null
/bin/rm -f $_WD/tmp/*

URL=$_c/mdm/universes/$_uni/records/query

curl -s -u $_u:$_p -X POST -H "Content-Type: application/xml" -d @$_WD/req.xml.init -o $_WD/tmp/out.xml $URL

if [ -f $_WD/tmp/out.xml ]
then
	xmllint -format $_WD/tmp/out.xml > $_WD/tmp/out.xml.proc
	/bin/cat $_WD/tmp/out.xml.proc > $_WD/tmp/GR.xml
fi

_offset=$(sed -n 2p $_WD/tmp/out.xml.proc | awk -F= '{ print $4 }' | cut -c2-5)
echo offset is $_offset > $_WD/offset.out

/bin/cp $_WD/req.xml.tmpl $_WD/req.xml
sed -i "s/{1}/$_offset/" $_WD/req.xml

while [ -n "$_offset" ]
do
	/bin/rm -f $_WD/tmp/$_offset.xml
	/bin/rm -f $_WD/tmp/$_offset.xml.proc
	curl -s -u $_u:$_p -X POST -H "Content-Type: application/xml" -d @$_WD/req.xml -o $_WD/tmp/$_offset.xml $URL
	if [ -f $_WD/tmp/$_offset.xml ]
	then
		xmllint -format $_WD/tmp/$_offset.xml > $_WD/tmp/$_offset.xml.proc
		/bin/cat $_WD/tmp/$_offset.xml.proc >> $_WD/tmp/GR.xml
	fi

	_offset=$(sed -n 2p $_WD/tmp/$_offset.xml.proc | awk '{ print $4 }' | cut -f2 -d'"')
	echo post offset is "$_offset"...

	/bin/cp $_WD/req.xml.tmpl $_WD/req.xml
	sed -i "s/{1}/$_offset/" $_WD/req.xml
	echo offset is $_offset >> $_WD/offset.out
done

if [ -f $_WD/tmp/GR.xml ]
then
	xml2csv --input $_WD/tmp/GR.xml --output $_WD/tmp/GR.csv --tag "Record"
fi
